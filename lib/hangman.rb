class Hangman
  attr_reader :guesser, :referee, :board
  def initialize(options = {})
    @guesser = options[:guesser]
    @referee = options[:referee]
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    guess = @guesser.guess
    position = @referee.check_guess(guess)
    self.update_board(guess, position)
    @guesser.handle_response
  end

  def update_board(guess, position)
    position.each do |idx|
      board[idx] = guess
    end
    board
  end

end

class HumanPlayer
end

class ComputerPlayer
  attr_reader :dictionary
  def initialize(dictionary)
    @dictionary = dictionary
  end

  def secret_word
    dictionary.sample
  end

  def pick_secret_word
    secret_word.length
  end

  def check_guess(letter)
    [].tap do |found_letters|
      secret_word.each_char.with_index do |ltr, idx|
        found_letters << idx if letter == ltr
      end
    end
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.select { |word| word.length == length}
  end

  def guess(board)
    dict_hash = Hash.new(0)
    @candidate_words.join.each_char do |letter|
      dict_hash[letter] += 1
    end
    dict_hash = dict_hash.sort_by { |key, value| value }.reverse
    dict_hash.each do |letter_value|
      unless board.include?(letter_value[0])
        return letter_value[0]
      end
    end
    puts "I have no more guesses."
  end

  def handle_response(guess, position)
    if position == []
      @candidate_words = @candidate_words.reject { |word| word.include?(guess) }
    else
      position.each do |spot|
        @candidate_words = @candidate_words.reject do |word|
          word[spot] != guess || position.count < word.count(guess)
        end
      end
    end
    @candidate_words
  end

  def candidate_words
    @candidate_words
  end

end
